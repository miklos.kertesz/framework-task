# AT in JS - Framework task

This is my solution for the Framework Task,
which is based on the result of WDIO Task#3.

## Prerequisites

1. Git installed
2. NodeJS installed

## List of required tasks / concepts

1. Page Object/Page Factory for page abstractions
2. Component objects of the required elements
3. Property files with test data for at least two different environments
4. Suites for smoke tests and other tests should be configured
5. If the test fails, a screenshot with the date and time is taken.


## Script scope of prerequisite WDIO Task#3

1. Opens the cloud pricing calculator page
2. Gets a price quote based on the parameters given in the task description
3. Opens Yopmail and generates a random temporary e-mail address
4. Sends the price quote to the temporary e-mail address then checks the inbox for the e-mail