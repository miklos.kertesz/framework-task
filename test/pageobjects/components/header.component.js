import BaseComponent from "./base.component.js";

export default class HeaderComponent extends BaseComponent {
    constructor() {
        super("devsite-header");
    }

}
