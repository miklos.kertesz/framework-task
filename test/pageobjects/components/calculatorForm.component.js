import BaseComponent from "../components/base.component.js";

export default class CalculatorFormComponent extends BaseComponent {

    constructor() {
        super("[name='ComputeEngineForm']");
    }

    select(option) {
        const selectorItems = {
            numberOfInstances: "[ng-model='listingCtrl.computeServer.quantity']",
            osSelector: "[ng-model='listingCtrl.computeServer.os']",
            provisioningMode: "[ng-model='listingCtrl.computeServer.class']",
            machineFamily: "[ng-model='listingCtrl.computeServer.family']",
            series: "[ng-model='listingCtrl.computeServer.series']",
            machineType: "[ng-model='listingCtrl.computeServer.instance']",
            gpuType: "[ng-model='listingCtrl.computeServer.gpuType']",
            storage: "[ng-model='listingCtrl.computeServer.ssd']",
            location: "[ng-model='listingCtrl.computeServer.location']",
            gpuCount: "[ng-model='listingCtrl.computeServer.gpuCount']",
            committedUsage: "[ng-model='listingCtrl.computeServer.cud']"
        }

        return this.rootEl.$(selectorItems[option]);
    }

    getOS(value) {
        const selectorItems = {
            free: "[value='free']",
            redHatEnterpriseLinux: "[value='rhel']"
        }
        return $(selectorItems[value]);
    }

    getProvisioningMode(value) {
        const selectorItems = {
            regular: "[value='regular']"
        }
        return $(selectorItems[value]);
    }

    getMachineFamily(value) {
        const selectorItems = {
            generalPurpose: "[value='gp']",
            computeOptimized: "[value='compute']"
        }
        return $(selectorItems[value]);
    }

    getMachineSeries(value) {
        const selectorItems = {
            n1: "[value='n1']",
            c2d: "[value='c2d']"
        }
        return $(selectorItems[value]);
    }

    getMachineType(value) {
        const selectorItems = {
            n1Standard8: "[value='CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-8']",
            c2dHigh112: "[value ='CP-COMPUTEENGINE-VMIMAGE-C2D-HIGHCPU-112']"
        }
        return $(selectorItems[value]);
    }

    getGPUType(value) {
        const selectorItems = {
            teslaT4: "[value='NVIDIA_TESLA_T4']" // task description requires V100, but it's not available for Frankfurt location
        }
        return $(selectorItems[value]);
    }

    getGPUQty(value) {
        const selectorItems = {
            oneGPU: "[ng-repeat='item in listingCtrl.supportedGpuNumbers[listingCtrl.computeServer.gpuType]'][value='1']"
        }
        return $(selectorItems[value]);
    }

    getSSD(value) {
        const selectorItems = {
            double: "[ng-repeat='item in listingCtrl.dynamicSsd.computeServer'][value='2']",
            octa: "[ng-repeat='item in listingCtrl.dynamicSsd.computeServer'][value='8']"
        }
        return $(selectorItems[value]);
    }

    getLocation(value) {
        const selectorItems = {
            frankfurt: "[ng-repeat='item in listingCtrl.fullRegionList | filter:listingCtrl.inputRegionText.computeServer'][value='europe-west3'] .md-text",
            montreal: "[ng-repeat='item in listingCtrl.fullRegionList | filter:listingCtrl.inputRegionText.computeServer'][value='northamerica-northeast1'] .md-text"
        }
        return $(selectorItems[value]);
    }

    getUsage(value) {
        const selectorItems = {
            oneYear: "#select_option_138",
            threeYears: "#select_option_139"
        }
        return $(selectorItems[value]);
    }

    get addGPUsCheckbox() {
        return $("[ng-model='listingCtrl.computeServer.addGPUs']");
    }

    get addToEstimateBtn() {
        return $("[ng-click='listingCtrl.addComputeServer(ComputeEngineForm);']")
    }

    // set number of instances to value
    async setNumberOfInstancesTo(value) {
        await this.select("numberOfInstances").setValue(value);
    }

    // set os to value
    async setOSTo(value) {
        await this.select("osSelector").click();
        await this.getOS(value).waitForDisplayed();
        await this.getOS(value).click();
    }

    // set provisioning mode to value
    async setProvisioningModeTo(value) {
        await this.select("provisioningMode").click();
        await this.getProvisioningMode(value).waitForDisplayed();
        await this.getProvisioningMode(value).click();
    }

    // set machine family to value
    async setMachineFamilyTo(value) {
        await this.select("machineFamily").click();
        await this.getMachineFamily(value).waitForDisplayed();
        await this.getMachineFamily(value).click();
    }

    // set series to value
    async setMachineSeriesTo(value) {
        await this.select("series").click();
        await this.getMachineSeries(value).waitForDisplayed();
        await this.getMachineSeries(value).click();
    }

    // set machine type to value
    async setMachineTypeTo(value) {
        await this.select("machineType").click();
        await this.getMachineType(value).waitForDisplayed();
        await this.getMachineType(value).click();
    }

    // set GPU to type, quantity
    async setGPUTo(type, qty) {
        await this.addGPUsCheckbox.click();
        await this.select("gpuType").click();
        await this.getGPUType(type).waitForDisplayed();
        await this.getGPUType(type).click();
        await this.select("gpuCount").click();
        await this.getGPUQty(qty).waitForDisplayed();
        await this.getGPUQty(qty).click();
    }

    // set ssd to value
    async setSSDTo(value) {
        await this.select("storage").click();
        await this.getSSD(value).waitForDisplayed();
        await this.getSSD(value).click();
    }

    // set location to value
    async setLocationTo(value) {
        await this.select("location").click();
        await this.getLocation(value).waitForDisplayed();
        await this.getLocation(value).click();
    }

    // set usage to value
    async setUsageTo(value) {
        await this.select("committedUsage").click();
        await this.getUsage(value).waitForDisplayed();
        await this.getUsage(value).click();
    }

    async addToEstimate() {
        await this.addToEstimateBtn.click();
    }
}