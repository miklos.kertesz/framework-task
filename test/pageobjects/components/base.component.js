export default class BaseComponent {
    constructor(rootSel) {
        this.rootSel = rootSel;
    }

    get rootEl() {
        return $(this.rootSel);
    }
}