import BaseComponent from "./base.component.js";

export default class EmailModalWindowComponent extends BaseComponent {

    constructor() {
        super("[name='emailForm']");
    }

    get emailInput() {
        return $("[ng-model='emailQuote.user.email']");
    }

    get submitEmailBtn() {
        return $("//*[contains(text(),'Send Email')]");
    }
}