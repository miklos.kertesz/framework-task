import { CalculatorFormComponent, CostEstimateComponent, EmailModalWindowComponent } from "../components/index.js"

export default class CostCalculatorPage {
    constructor() {
        this.calculatorForm = new CalculatorFormComponent();
        this.costEstimate = new CostEstimateComponent();
        this.emailModalWindow = new EmailModalWindowComponent();
    }
}
