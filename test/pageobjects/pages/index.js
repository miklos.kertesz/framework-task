import BasePage from "./base.page.js";
import CostCalculatorPage from "./costCalculator.page.js";

function createPage(page) {
    const pages = {
        basePage: new BasePage(),
        costCalculatorPage: new CostCalculatorPage()
    }
    return pages[page];
}

export {
    BasePage,
    CostCalculatorPage,
    createPage
};
