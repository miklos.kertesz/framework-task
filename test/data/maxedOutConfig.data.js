export const options = {
    numberOfInstances: "512",
    osSelector: "redHatEnterpriseLinux",
    provisioningMode: "regular",
    machineFamily: "computeOptimized",
    machineSeries: "c2d",
    machineType: "c2dHigh112",
    storage: "octa",
    location: "montreal",
    committedUsage: "threeYears"
}

export const expected = {
    title: "Cloud Pricing Calculator",
    numberOfInstances: "512",
    os: "Paid",
    provisioningMode: "Regular",
    instanceType: "c2d-highcpu-112",
    storage: "8x375 GiB",
    location: "Montreal",
    commitment: "Commitment term: 3 Years"
}