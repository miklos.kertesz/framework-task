export const options = {
    numberOfInstances: "4",
    osSelector: "free",
    provisioningMode: "regular",
    machineFamily: "generalPurpose",
    machineSeries: "n1",
    machineType: "n1Standard8",
    gpuType: "teslaT4",
    storage: "double",
    location: "frankfurt",
    gpuCount: "oneGPU",
    committedUsage: "oneYear"
}

export const expected = {
    title: "Cloud Pricing Calculator",
    numberOfInstances: "4",
    os: "Free",
    provisioningMode: "Regular",
    instanceType: "n1-standard-8",
    gpu: "1 NVIDIA TESLA T4",
    storage: "2x375 GiB",
    location: "Frankfurt",
    commitment: "Commitment term: 1 Year"
}