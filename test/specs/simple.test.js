import { createPage } from "../pageobjects/pages/index.js";
import fs from "fs";
import { options as task3Options, expected as task3ExpectedValues } from "../data/task3.data.js";
import { options as maxedOutOptions, expected as maxedOutExpectedValues } from "../data/maxedOutConfig.data.js";

/*
// my solution below did not work for having the parameters
// imported from a json file, so i implemented importing them
// from javascript objects instead due to the task submission
// deadline being too close to spend sufficient time for
// debugging
//
// left it here, so that I can try to debug it later 

// read test scenario 
fs.readFile('../data/task3.data.json', 'utf8', (err, data) => {
    if (err) {
        console.error('Error reading file:', err);
        return;
    }

    // parse json data
    try {
        const options = JSON.parse(data);
        console.log('Options:', options);
    } catch (error) {
        console.error('JSON parsing error:', error);
    }
}); */

describe('Google Cloud Pricing Calculator', () => {

    beforeEach(async () => {
        await createPage("basePage").open();
    });

    it('Checks page title', async () => {
        await expect(browser).toHaveTitle(task3ExpectedValues.title);
    });

    it('Gets a price quotation for the configuration in Task#3 and verify the result', async () => {

        await createPage("costCalculatorPage").calculatorForm.setNumberOfInstancesTo(task3Options.numberOfInstances);
        await createPage("costCalculatorPage").calculatorForm.setOSTo(task3Options.osSelector);
        await createPage("costCalculatorPage").calculatorForm.setProvisioningModeTo(task3Options.provisioningMode);
        await createPage("costCalculatorPage").calculatorForm.setMachineFamilyTo(task3Options.machineFamily);
        await createPage("costCalculatorPage").calculatorForm.setMachineSeriesTo(task3Options.machineSeries);
        await createPage("costCalculatorPage").calculatorForm.setMachineTypeTo(task3Options.machineType);
        await createPage("costCalculatorPage").calculatorForm.setGPUTo(task3Options.gpuType, task3Options.gpuCount);
        await createPage("costCalculatorPage").calculatorForm.setSSDTo(task3Options.storage);
        await createPage("costCalculatorPage").calculatorForm.setLocationTo(task3Options.location);
        await createPage("costCalculatorPage").calculatorForm.setUsageTo(task3Options.committedUsage);
        await createPage("costCalculatorPage").calculatorForm.addToEstimate();

        // verify automated input results
        await expect(createPage("costCalculatorPage").costEstimate.item("numberOfInstances")).toHaveTextContaining(task3ExpectedValues.numberOfInstances);
        await expect(createPage("costCalculatorPage").costEstimate.item("location")).toHaveTextContaining(task3ExpectedValues.location);
        await expect(createPage("costCalculatorPage").costEstimate.item("commitment")).toHaveTextContaining(task3ExpectedValues.commitment);
        await expect(createPage("costCalculatorPage").costEstimate.item("provisioningModel")).toHaveTextContaining(task3ExpectedValues.provisioningMode);
        await expect(createPage("costCalculatorPage").costEstimate.item("instanceType")).toHaveTextContaining(task3ExpectedValues.instanceType);
        await expect(createPage("costCalculatorPage").costEstimate.item("os")).toHaveTextContaining(task3ExpectedValues.os);
        await expect(createPage("costCalculatorPage").costEstimate.item("gpu")).toHaveTextContaining(task3ExpectedValues.gpu);
        await expect(createPage("costCalculatorPage").costEstimate.item("ssd")).toHaveTextContaining(task3ExpectedValues.storage);

    });

    it('Gets a price quotation for a maxed out configuration and verify the result', async () => {

        await createPage("costCalculatorPage").calculatorForm.setNumberOfInstancesTo(maxedOutOptions.numberOfInstances);
        await createPage("costCalculatorPage").calculatorForm.setOSTo(maxedOutOptions.osSelector);
        await createPage("costCalculatorPage").calculatorForm.setProvisioningModeTo(maxedOutOptions.provisioningMode);
        await createPage("costCalculatorPage").calculatorForm.setMachineFamilyTo(maxedOutOptions.machineFamily);
        await createPage("costCalculatorPage").calculatorForm.setMachineSeriesTo(maxedOutOptions.machineSeries);
        await createPage("costCalculatorPage").calculatorForm.setMachineTypeTo(maxedOutOptions.machineType);
        await createPage("costCalculatorPage").calculatorForm.setSSDTo(maxedOutOptions.storage);
        await createPage("costCalculatorPage").calculatorForm.setLocationTo(maxedOutOptions.location);
        await createPage("costCalculatorPage").calculatorForm.setUsageTo(maxedOutOptions.committedUsage);
        await createPage("costCalculatorPage").calculatorForm.addToEstimate();

        // verify automated input results
        await expect(createPage("costCalculatorPage").costEstimate.item("numberOfInstances")).toHaveText(maxedOutExpectedValues.numberOfInstances, { containing: true });
        await expect(createPage("costCalculatorPage").costEstimate.item("location")).toHaveText(maxedOutExpectedValues.location, { containing: true });
        await expect(createPage("costCalculatorPage").costEstimate.item("commitment")).toHaveText(maxedOutExpectedValues.commitment, { containing: true });
        await expect(createPage("costCalculatorPage").costEstimate.item("provisioningModel")).toHaveText(maxedOutExpectedValues.provisioningMode, { containing: true });
        await expect(createPage("costCalculatorPage").costEstimate.item("instanceType")).toHaveText(maxedOutExpectedValues.instanceType, { containing: true });
        await expect(createPage("costCalculatorPage").costEstimate.item("os")).toHaveText(maxedOutExpectedValues.os, { containing: true });
        await expect(createPage("costCalculatorPage").costEstimate.item("ssd")).toHaveText(maxedOutExpectedValues.storage, { containing: true });
    });

});