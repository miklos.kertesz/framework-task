import { createPage } from "../pageobjects/pages/index.js";
import { options as smokeTestOptions, expected as smokeTestExpectedValues } from "../data/smokeTest.data.js";

describe('Google Cloud Pricing Calculator', () => {

    beforeEach(async () => {
        await createPage("basePage").open();
    });

    it('Checks page title', async () => {
        await expect(browser).toHaveTitle(smokeTestExpectedValues.title);
    });

});
